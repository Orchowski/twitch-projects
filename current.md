# BILLS!
We're all love bills. That's the most loveable thing in the world :heart:
Easy, easy. I'm only joking, but truth is that we all have to manage this mess somehow. I believe that I can create a tool that will help me with that.

## For what this project is?
It's my first contact with the python programming language and I want to learn it. The goal is to create a web page with a backend for monitoring and alerting about upcoming personal bills. Maybe also some web-scraping will be included to watch financial assets and also currency rates.

