# AlexTheSoftwareDev - TWITCH

[[_TOC_]]

## Why are you doing this? 

- To learn something new
- To help me stay organized
- To interact with the community
- To keep me motivated. 
 

## Who you are?
 My name is Alex and I come from Poland. I am a programmer for about 4 years on different levels, positions, and technologies. Focused on being a full-stack - technologies don't matter. The business value this is it!

## Technology stack
- JVM
    - Languages
        - Java
        - Kotlin
        - Groovy
    - Spring
- .NET
    - Languages
        - C#
        - F# (experimentally for a moment)
    - Desktop apps with:
        - UWP (and mobile actually)
        - WPF
    - Backend with 
        - ASP .NET CORE
        - ASP .NET MVC
- DevOps
    - Docker
    - K8s
    - CI/CD with Jenkins etc.
    - Terraform
    - Ansible
- Frontend (I'm not an expert on this, unfortunately)
    - Angular
    - VueJS
    - Microfrontends evangelist

## Projects
### [#1 Bills project - learning python](bills-project.md)    
It's my first contact with the python programming language. The goal is to create a web page with a backend for monitoring and alerting about upcoming personal bills. Maybe also some web-scraping will be included to watch financial assets and also currency rates.