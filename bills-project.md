# BILLS!

[[_TOC_]]


We're all love bills. That's the most loveable thing in the world :heart:

Easy, easy. I'm only joking, but truth is that we all have to manage this mess somehow. I believe that I can create a tool that will help me with that.

## For what this project is?
It's my first contact with the python programming language and I want to learn it. The goal is to create a web page with a backend for monitoring and alerting about upcoming personal bills. Maybe also some web-scraping will be included to watch financial assets and also currency rates.

## [:point_right: REPO IS HERE :point_left:](https://gitlab.com/Orchowski/bills)


## Features

- List of bills
- Modifying values in each bill on the list
- Marking as payed
- Bills are repeating with given time on X day of the month
- "Future feature" generating payments for X times. eg. car installment each month 1000PLN for 3 years. After that bill will be deactivated.
- Notes to a bill (a type of bill, adding categories/projects etc. ) 
- Upcoming payments ( list of the payments generated from bill definition I have to pay in 10 days) 
